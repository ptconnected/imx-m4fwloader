cmake_minimum_required(VERSION 3.16)
project(mdc_m4fwloader C)

set(CMAKE_C_STANDARD 11)

#include_directories("/home/skubesch/Documents/mdc_tools/include/")

add_executable(mdc_m4fwloader m4fwloader.c)

install (TARGETS mdc_m4fwloader RUNTIME DESTINATION bin)
